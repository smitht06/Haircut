#define lists for prices and hairstyles
hairstyles = ["bouffant", "pixie", "dreadlocks", "crew", "bowl", "bob", "mohawk", "flattop"]

prices = [30, 25, 40, 20, 20, 35, 50, 35]

last_week = [2, 3, 5, 8, 4, 4, 6, 2]

total_price = 0

#use for loop to get total of all prices
for price in prices:
  total_price += price

#get average price and lower prices by 5
average_price = total_price/len(prices)
print("Average Haircut Price: " + str(average_price))
new_prices = [price - 5 for price in prices]
print(new_prices)

#get total weekly revenue
total_revenue = 0;
for i in range(len(hairstyles)):
  total_revenue += prices[i] * last_week[i]
print("Total revenue: " + str(total_revenue))

#get average daily revenue
average_daily_revenue = total_revenue/7
print("Average daily revenue: " + str(average_daily_revenue))

#get all styles under 30 dollars
cuts_under_30 = [
  hairstyles[i] for i in range(len(hairstyles)) if new_prices[i] < 30]
print(cuts_under_30)
